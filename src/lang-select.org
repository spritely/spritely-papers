#+begin_export html
<form class="lang-select" onchange="onLangChange(event, this)">
  <input type="radio" name="lang" value="scheme" id="scheme-radio">
  <label for="scheme-radio">Scheme</label>
  <input type="radio" name="lang" value="wisp" id="wisp-radio">
  <label for="wisp-radio">Wisp</label>
</form>
#+end_export
