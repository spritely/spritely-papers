(define packages
  '(emacs
    emacs-org
    emacs-wisp-mode
    emacs-htmlize
    guile
    guile-wisp
    guile-goblins
    libreoffice
    gzip
    unzip
    make))

(specifications->manifest
 (map symbol->string
      packages))
