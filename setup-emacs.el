(require 'seq)

(setq inhibit-message-regexps '("All your packages are already installed"
                                "Setting up indent for shell type"
                                "Indentation variables are now local."
                                "Indentation setup for shell type")

      ;; package.el
      package-selected-packages '(htmlize wisp-mode)
      package-load-list '((htmlize t) (wisp-mode t))
      
      ;; ODT export
      org-odt-create-custom-styles-for-srcblocks nil

      ;; HTML export
      org-html-doctype "html5"
      org-html-htmlize-output-type 'css
      org-html-postamble t
      org-html-postamble-format
      '(("en" "<p>Author: %a</p><p>Created: %d</p><p>Last modified: %C</p>")))

(define-advice message (:before-until (format &rest _))
  "Filter messages that match any element of `inhibit-message-regexps'."
  (seq-find (lambda (rx) (string-match-p rx format))
            inhibit-message-regexps))

(package-initialize)
