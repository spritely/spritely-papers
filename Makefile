# -*- indent-tabs-mode: t -*-
MAKEFLAGS = -j $(shell nproc) -O
ROOT != pwd
SHELL = emacs
.SHELLFLAGS = -Q --batch -l setup-emacs.el --eval

PAPERS = spritely-framing spritely-core spritely-for-users
PAPERS += scheme-primer scheme-primer.de
PAPERS += petnames implementation-of-petname-system-in-existing-chat-app
FORMATS = org html odt pdf

EXAMPLES = goblins-blog simple-sealers taste-of-goblins
LANGUAGES = scm w
EXAMPLE_FILES = $(foreach e,$(EXAMPLES),$(foreach l,$(LANGUAGES),public/$(e).$(l)))

GUILE_LOAD_PATH := $(ROOT)/public:$(GUILE_LOAD_PATH)

.ONESHELL:
.NOTINTERMEDIATE:
.PHONY: all check tangle clean

all: $(PAPERS) $(EXAMPLE_FILES)

check: $(wildcard test/*)

tangle: $(EXAMPLE_FILES)

clean:
	(delete-directory "public" t)

$(PAPERS): % : $(addprefix public/%.,$(FORMATS))

public:
	(mkdir "$@" t)

public/%.org: src/%.org | public
	(progn
	  (find-file "$<")
	  (rename-file (org-org-export-to-org) "$(ROOT)/$@" t))

public/main.css: src/main.css | public
	(copy-file "$<" "$@" t t)

public/imgs: src/imgs | public
	(copy-directory "$<" "$@" t t t)

public/%.html: src/%.org public/main.css public/imgs | public
	(progn
	  (find-file "$<")
	  (rename-file (org-html-export-to-html) "$(ROOT)/$@" t))

public/%.odt: src/%.org | public
	(progn
	  (find-file "$<")
	  (rename-file (org-odt-export-to-odt) "$(ROOT)/$@" t))

public/%.pdf: public/%.odt | public
	(shell-command "libreoffice --convert-to pdf --outdir public $<")

$(EXAMPLE_FILES) &: src/spritely-core.org | public
	(progn
	  (find-file "$<")
	  ;; Trick emacs to writing all tangled files to public folder
	  (setq org-babel-pre-tangle-hook nil
		buffer-file-name "$(ROOT)/public/tmp")
	  (org-babel-tangle))

test/%: $(EXAMPLE_FILES)
	(shell-command "guile $@")
